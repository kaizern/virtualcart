package com.virtualcart;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.virtualcart.R;
import com.virtualcart.constant.StringConstants;
import com.virtualcart.productlist.Product;
import com.virtualcart.productlist.ProductAdapter;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.SyncStateContract.Constants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ProductListActivity extends Activity {
	private static final int BARCODE_SCAN_RESULT = 0;
	private static final int CHECKBOX_DECISION_RESULT = 1;
	private static final int PAYMENT_DECISION_RESULT = 2;

	private ListView productList;
	private List<Product> productsInCart;
	private ProductAdapter productListAdapter;
	private CheckBox autoAddCheckBox;
	private Drawable emptyDrawable;
	private Drawable drawable;
	private RelativeLayout rLayout;
	private Resources res;
	private TextView footerTextBox;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.productlist);


		rLayout = (RelativeLayout) findViewById(R.id.relativeListLayout);
		res = getResources(); // resource handle
		emptyDrawable = res.getDrawable(R.drawable.cart_empty);
		drawable = res.getDrawable(R.drawable.background);

		footerTextBox = (TextView) findViewById(R.id.footer_1);

		if (savedInstanceState != null) {
			Object obj = savedInstanceState
					.getParcelableArrayList("productsInCart");
			if (obj != null) {
				productsInCart = (List<Product>) obj;
			}
			String overallSum = savedInstanceState.getString("overallSum");
			if (overallSum != null) {
				getOverallPrice().setText(overallSum);
			}
		} else {
			productsInCart = new ArrayList<Product>();
			getOverallPrice().setText("0");
		}

		if(productsInCart.size()==0) {
			toggleVisibilityEmptyCart(false);
		}

		productListAdapter = new ProductAdapter(this,
				R.layout.productlist_item_row, productsInCart, true);

		productList = (ListView) findViewById(R.id.productlistview);

		/*
		 * View footer =
		 * getLayoutInflater().inflate(R.layout.listview_footer_row, null);
		 * productList.addFooterView(footer);
		 */

		productList.setAdapter(productListAdapter);

		initListeners();
	}

	private BigDecimal updateOverallPrice() {

		Iterator<Product> it = productsInCart.iterator();
		BigDecimal overallPrice = new BigDecimal("0");
		while (it.hasNext()) {
			Product current = it.next();
			overallPrice = overallPrice.add(current.getPrice());
		}

		TextView price = getOverallPrice();

		price.setText(String.valueOf(overallPrice));

		return overallPrice;
	}

	public TextView getOverallPrice() {
		TextView price = (TextView) findViewById(R.id.footer_2);
		return price;
		// return (String)price.getText();
	}

	public void setOverallPrice() {

	}

	private void initListeners() {
		findViewById(R.id.barcodescan).setOnClickListener(
				new View.OnClickListener() {

					public void onClick(View arg0) {
						Intent intent = new Intent(
								"com.google.zxing.client.android.SCAN");
						intent.putExtra(
								"com.google.zxing.client.android.SCAN.SCAN_MODE",
								"QR_CODE_MODE");
						intent.putExtra(
								"com.google.zxing.client.android.SCAN.SCAN_MODE",
								"PRODUCT_MODE");
						startActivityForResult(intent, 0);
					}
				});

		autoAddCheckBox = (CheckBox) findViewById(R.id.autoAddCheckBox);

		Button payButton = (Button) findViewById(R.id.pay);
		payButton.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Intent paymentPreviewActivity = new Intent(
						getApplicationContext(), PaymentPreviewActivity.class);
				paymentPreviewActivity.putParcelableArrayListExtra(
						"productsInCart", (ArrayList<Product>) productsInCart);
				paymentPreviewActivity.putExtra("totalPrice", getOverallPrice()
						.getText());
				startActivityForResult(paymentPreviewActivity,
						PAYMENT_DECISION_RESULT);
				// create layout with list and button (+insert selver card id)
			}
		});

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {

		// for barcode scan
		if (requestCode == BARCODE_SCAN_RESULT) {
			if (resultCode == RESULT_OK) {
				String contents = intent
						.getStringExtra(StringConstants.SCAN_RESULT);
				// String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
				if (!autoAddCheckBox.isChecked()) {
					Intent productDetailsScreen = new Intent(
							getApplicationContext(), ProductDescActivity.class);
					productDetailsScreen.putExtra("barcodeResult", contents);
					// productDetailsScreen.putExtra("barcodeFormat", format);
					startActivityForResult(productDetailsScreen,
							CHECKBOX_DECISION_RESULT);
				} else {
					toggleVisibilityEmptyCart(true);
					Product tempProduct = new Product(contents, "TEST",
							new BigDecimal("19"));
					tempProduct.getPrice();
					productsInCart.add(tempProduct);
					calculateNewOverall(tempProduct);
					productListAdapter.notifyDataSetChanged();
				}

				// sendBroadcast(productDetailsScreen);
			} else if (resultCode == RESULT_CANCELED) {
				// Handle cancel
			}
		}

		// for checkbox handling
		if (requestCode == CHECKBOX_DECISION_RESULT) {
			String contents = null;
			if (intent != null) {
				if ((contents = intent
						.getStringExtra(StringConstants.DECISION_RESULT_DEFINITION)) != null) {
					toggleVisibilityEmptyCart(true);
					Product tempProduct = new Product(contents, "TEST",
							new BigDecimal("19"));
					productsInCart.add(tempProduct);
					calculateNewOverall(tempProduct);
					productListAdapter.notifyDataSetChanged();
				}
			}
		}
	}

	private void toggleVisibilityEmptyCart(boolean action) {
		if(action) {
			rLayout.setBackgroundDrawable(drawable);
			getOverallPrice().setVisibility(View.VISIBLE);
			footerTextBox.setVisibility(View.VISIBLE);
		}
		else {
			rLayout.setBackgroundDrawable(emptyDrawable);
			getOverallPrice().setVisibility(View.GONE);
			footerTextBox.setVisibility(View.GONE);
		}

	}

	private void calculateNewOverall(Product tempProduct) {
		BigDecimal currentOverallPrice = new BigDecimal(
				(String) getOverallPrice().getText());
		BigDecimal modifiedOverallPrice = currentOverallPrice.add(tempProduct
				.getPrice());
		getOverallPrice().setText(String.valueOf(modifiedOverallPrice));
	}

	@Override
	protected void onSaveInstanceState(Bundle savedInstanceState) {
		savedInstanceState.putParcelableArrayList(
				StringConstants.PRODUCTS_IN_CART,
				(ArrayList<Product>) productsInCart);
		savedInstanceState.putString("overallSum", (String) getOverallPrice()
				.getText());
		super.onSaveInstanceState(savedInstanceState);

	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		productsInCart = savedInstanceState
				.getParcelableArrayList(StringConstants.PRODUCTS_IN_CART);
		getOverallPrice().setText(savedInstanceState.getString("overallSum"));

	}

}

