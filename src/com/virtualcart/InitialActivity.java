package com.virtualcart;

import com.virtualcart.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created with IntelliJ IDEA.
 * User: Antonve
 * Date: 10/17/12
 * Time: 7:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class InitialActivity extends Activity {

    @Override
    public void onCreate(Bundle icicle)
    {
        super.onCreate(icicle);
        setContentView(R.layout.firstscreen);

        Button btnNextScreen = (Button) findViewById(R.id.NewShopping);


        btnNextScreen.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                //Starting a new Intent
                Intent nextScreen = new Intent(getApplicationContext(), ProductListActivity.class);

                //Sending data to another Activity

               // Log.e("n", inputName.getText() + "." + inputEmail.getText());

                startActivity(nextScreen);

            }
        });
    }
}
