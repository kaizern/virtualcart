package com.virtualcart.model;

import android.widget.Button;
import android.widget.TextView;

public class ProductHolder {
	TextView txtTitle;
	TextView txtAmount;
	Button test;

	public TextView getTxtTitle() {
		return txtTitle;
	}
	public void setTxtTitle(TextView txtTitle) {
		this.txtTitle = txtTitle;
	}
	public TextView getTxtAmount() {
		return txtAmount;
	}
	public void setTxtAmount(TextView txtAmount) {
		this.txtAmount = txtAmount;
	}
	public Button getTest() {
		return test;
	}
	public void setTest(Button test) {
		this.test = test;
	}


	/*ImageView imgIconDec;
	ImageView imgIconAct;
	ImageView imgIconInc;*/

}
