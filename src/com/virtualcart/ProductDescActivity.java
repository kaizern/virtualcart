package com.virtualcart;

import com.virtualcart.R;
import com.virtualcart.constant.StringConstants;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ProductDescActivity extends Activity {

	Button backToCart, addToCart;
	String barcodeResult;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.productdesc);

		TextView txtBarcodeResult = (TextView) findViewById(R.id.barcodeResult);
		TextView txtBarcodeFormat = (TextView) findViewById(R.id.barcodeFormat);
		addToCart = (Button) findViewById(R.id.addToCart);
		backToCart = (Button) findViewById(R.id.backToCart);

		initButtonListeners();

		Intent i = getIntent();
		// Receiving the Data
		barcodeResult = i.getStringExtra(StringConstants.BARCODE_RESULT);
		String barcodeFormat = i.getStringExtra(StringConstants.BARCODE_FORMAT);
		Log.e("Second Screen", barcodeResult + "." + barcodeFormat);

		// Displaying Received data
		txtBarcodeResult.setText(barcodeResult);
		txtBarcodeFormat.setText(barcodeFormat);

	}

	private void initButtonListeners() {
		addToCart.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				Intent returnIntent = new Intent();
				returnIntent.putExtra(StringConstants.DECISION_RESULT_DEFINITION,barcodeResult);
				setResult(1,returnIntent);
				finish();
			}
		});

		backToCart.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				finish();
			}
		});

	}

}
