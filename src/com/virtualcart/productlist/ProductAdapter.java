package com.virtualcart.productlist;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.opengl.Visibility;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.virtualcart.ProductListActivity;
import com.virtualcart.R;
import com.virtualcart.service.ProductService;
import com.virtualcart.service.mock.MockProductService;

public class ProductAdapter extends BaseAdapter {

	Context context;
	int layoutResourceId;
	ProductAdapter adapter;
	List<Product> data = null;
	private boolean buttonsVisible;
	public static final String SUBTRACT = "substract";
	public static final String ADD = "add";

	public ProductAdapter(Context context, int layoutResourceId,
			List<Product> data, boolean buttonsVisible) {
		super();
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
		this.buttonsVisible = buttonsVisible;
		adapter = this;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View row = convertView;
		ProductHolder holder = null;

		final Product product = data.get(position);

		if (row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);

			final TextView price = (TextView) (((Activity) context).findViewById(R.id.footer_2));

			holder = new ProductHolder();
			final TextView txtAmount = (TextView) row
					.findViewById(R.id.txtAmount);
			txtAmount.setText("1");
			Button minusButton = (Button) row.findViewById(R.id.buttonMinus);
			Button plusButton = (Button) row.findViewById(R.id.buttonPlus);
			final LinearLayout accordionContent = (LinearLayout)row.findViewById(R.id.panel1);

			holder.txtTitle = (TextView) row.findViewById(R.id.txtTitle);
			holder.minusButton = minusButton;
			holder.txtAmount = txtAmount;
			holder.plusButton = plusButton;
			holder.accordionContent = accordionContent;

			row.setTag(holder);
			row.setId(position);

			minusButton.setId(position);
			final int id = minusButton.getId();

			if(buttonsVisible) {
				minusButton.setOnClickListener(new View.OnClickListener() {
					public void onClick(View arg0) {
						int currAmount = Integer.parseInt(String.valueOf(txtAmount
								.getText()));

						int position = data.indexOf(product);
						if (currAmount >= 1) {
							txtAmount.setText(String.valueOf(currAmount - 1));
							recalculateOverallPrice(product, price, SUBTRACT);
						} else {
							data.remove(position);
							notifyDataSetChanged();
							recalculateOverallPrice(product, price, SUBTRACT);
						}
					}


				});

				plusButton.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						int currAmount = Integer.parseInt(String.valueOf(txtAmount
								.getText()));
						txtAmount.setText(String.valueOf(currAmount + 1));
						recalculateOverallPrice(product, price, ADD);
					}
				});
			}
			else {
				minusButton.setVisibility(View.GONE);
				plusButton.setVisibility(View.GONE);
				txtAmount.setVisibility(View.GONE);
			}

			accordionContent.setVisibility(View.GONE);

			CheckBox upButton = (CheckBox) row.findViewById(R.id.imgIconUp);
			upButton.setOnCheckedChangeListener(new OnCheckedChangeListener()
			{
			    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
			    {
			        if ( isChecked )
			        {
			        	accordionContent.setVisibility(View.VISIBLE);
			        }
			        else {
			        	accordionContent.setVisibility(View.GONE);
			        }
			    }
			});



		} else {
			holder = (ProductHolder) row.getTag();
		}
		ProductService productService = new MockProductService();

		//if(product.barcode)
		Product actProduct = productService.getProductById(Long
				.parseLong(product.barcode));
		holder.txtTitle.setText(actProduct.getName());
		return row;
	}


	private void recalculateOverallPrice(final Product product,
			final TextView price, String operation) {
		BigDecimal currentOverallPrice = new BigDecimal((String) price.getText());
		BigDecimal modifiedOverallPrice;
		if(operation.equals("substract")) {
			modifiedOverallPrice = currentOverallPrice.subtract(product.getPrice());
		}
		else {
			modifiedOverallPrice = currentOverallPrice.add(product.getPrice());
		}

		price.setText(String.valueOf(modifiedOverallPrice));
	}

	static class ProductHolder {
		LinearLayout accordionContent;
		TextView txtAmount;
		TextView txtTitle;
		BigDecimal price;
		Button upButton;
		Button minusButton;
		Button plusButton;

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

}
