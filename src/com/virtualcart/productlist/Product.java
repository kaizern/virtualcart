package com.virtualcart.productlist;

import java.math.BigDecimal;

import android.os.Parcel;
import android.os.Parcelable;

public class Product implements Parcelable {

	public String barcode;
	public String name;
	public BigDecimal price;

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Product() {
		super();
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Product(String barcode, String name, BigDecimal price) {
		super();
		this.name = name;
		this.barcode = barcode;
		this.price = price;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	 public Product(Parcel in) {
		 	barcode = in.readString();
	        name = in.readString();
	    }

	@Override
	public void writeToParcel(Parcel arg0, int arg1) {
		arg0.writeString(barcode);
		arg0.writeString(name);

	}

	 public static final Parcelable.Creator<Product> CREATOR = new Parcelable.Creator<Product>() {
	        public Product createFromParcel(Parcel in) {
	            return new Product(in);
	        }

	        public Product[] newArray(int size) {
	            return new Product[size];
	        }
	    };

}
