package com.virtualcart.constant;

public interface StringConstants {

	static final String DECISION_RESULT_DEFINITION = "DECISION_RESULT";
	static final String BARCODE_RESULT = "barcodeResult";
	static final String BARCODE_FORMAT = "barcodeFormat";
	static final String PRODUCTS_IN_CART = "productsInCart";
	static final String SCAN_RESULT = "SCAN_RESULT";
}

