package com.virtualcart.service;

import com.virtualcart.productlist.Product;

public interface ProductService {

	public Product getProductById(Long productId);

}
