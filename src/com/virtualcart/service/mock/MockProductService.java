package com.virtualcart.service.mock;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import com.virtualcart.productlist.Product;
import com.virtualcart.service.ProductService;

public class MockProductService implements ProductService {

	public static Map<Long, Product> productsMap;

	public MockProductService() {
		initProductsMap();
	}

	private void initProductsMap() {
		productsMap = new HashMap<Long, Product>();
		productsMap.put(new Long("1111221132134681"), new Product(
				"1111221132134681", "TALLINK CARD", new BigDecimal("19")));
		/*
		 * productsList.add(new Product("1111221132134681","TALLINK CARD"));
		 * productsList.add(new Product("1111221132134681","TALLINK CARD"));
		 * productsList.add(new Product("1111221132134681","TALLINK CARD"));
		 * productsList.add(new Product("1111221132134681","TALLINK CARD"));
		 */
	}

	@Override
	public Product getProductById(Long productId) {
		Product scannedProduct = productsMap.get(productId);
		if (scannedProduct != null) {
			return scannedProduct;
		} else {
			return productsMap.get(Long.valueOf("1111221132134681"));
		}
	}

}
