package com.virtualcart;

import java.util.ArrayList;
import java.util.List;

import com.virtualcart.constant.StringConstants;
import com.virtualcart.productlist.Product;
import com.virtualcart.productlist.ProductAdapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class PaymentPreviewActivity extends Activity  {

	Button backToCart, proceedToPayment;
	String barcodeResult;
	private ListView productList;
	private List<Product> productsInCart;
	private ProductAdapter productListAdapter;
	int selectedItem = 0;
	String[] letters = new String[] { "SEB", "SWED", "VISA", "MASTERCARD" };
	PaymentPreviewActivity thisActivity;
	String totalPrice;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		thisActivity = this;
		setContentView(R.layout.payment_preview);

		Intent i = getIntent();
		// Receiving the Data
		productsInCart = i.getParcelableArrayListExtra("productsInCart");
		totalPrice = i.getStringExtra("totalPrice");

		if (productsInCart == null) {
			productsInCart = new ArrayList<Product>();
		}
		if(totalPrice==null)
		{
			totalPrice = "0";
		}



		/*
		 * if (savedInstanceState != null &&
		 * savedInstanceState.getParcelableArrayList("productsInCart") != null)
		 * { productsInCart = savedInstanceState
		 * .getParcelableArrayList("productList"); } else { productsInCart = new
		 * ArrayList<Product>(); }
		 */

		proceedToPayment = (Button) findViewById(R.id.proceedToPayment);
		backToCart = (Button) findViewById(R.id.backToCart);

		initButtonListeners();

		productListAdapter = new ProductAdapter(this,
				R.layout.productlist_item_row, productsInCart, false);

		productList = (ListView) findViewById(R.id.productlistview);

		final TextView price = (TextView)findViewById(R.id.footer_2);
		price.setText(totalPrice);

		/*
		 * View header = (View) getLayoutInflater().inflate(
		 * R.layout.listview_header_row, null);
		 * productList.addHeaderView(header);
		 */

		/*
		 * View footer =
		 * getLayoutInflater().inflate(R.layout.listview_footer_row, null);
		 * productList.addFooterView(footer);
		 */

		productList.setAdapter(productListAdapter);


		// Receiving the Data

		// Displaying Received data

	}

	private void initButtonListeners() {
		/*
		 * proceedToPayment.setOnClickListener(new View.OnClickListener() {
		 *
		 * public void onClick(View arg0) { Intent returnIntent = new Intent();
		 * setResult(2, returnIntent); finish(); } });
		 */

		backToCart.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				finish();
			}
		});

		proceedToPayment.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(thisActivity);
				builder.setTitle("Select One Letter");
				int selected = selectedItem;
				builder.setSingleChoiceItems(letters, selected,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								selectedItem = which;
								Toast.makeText(
										PaymentPreviewActivity.this,
										"Valisid "
												+ letters[selectedItem] + " maksmise",
										Toast.LENGTH_SHORT).show();
								dialog.dismiss();
							}
						});
				AlertDialog alert = builder.create();
				alert.show();
			}

		});

	}
}
